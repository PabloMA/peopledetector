CREATE TABLE SpaceActivity (
    SpaceId INT NOT NULL,
    Count INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	CONSTRAINT FK_SpaceActivity_Spaces FOREIGN KEY (SpaceId)
	REFERENCES Spaces(SpaceId)
);