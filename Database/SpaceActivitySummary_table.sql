CREATE TABLE SpaceActivitySummary (
    SpaceId INT NOT NULL,
    CurrentCount INT NOT NULL,
	MaxCountLastHour INT NOT NULL,
	MaxCount INT NOT NULL,
	LastUpdate DATETIME NOT NULL,
    CONSTRAINT PK_SpaceActivitySummary_SpaceId PRIMARY KEY CLUSTERED (SpaceId),
	CONSTRAINT FK_SpaceActivitySummary_Spaces FOREIGN KEY (SpaceId)
	REFERENCES Spaces(SpaceId)
);