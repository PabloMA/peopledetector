﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace Cyclops
{
    class Program
    {
        private static string _imageEndpoint;
        private static string _spaceId;
        public static bool _useBackgroundSubstraction;

        static void Main()
        {
            MainAsync().GetAwaiter().GetResult();
        }

        //EXECUTE FORM TERMINAL, PERMISSION ISSUES
        static async Task MainAsync()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");

            IConfigurationRoot configuration = builder.Build();

            _spaceId = configuration.GetSection("SpaceId").Value;
            _imageEndpoint = configuration.GetSection("EndPoint").Value;
            _useBackgroundSubstraction = bool.Parse(configuration.GetSection("UseBackgroundSubstraction").Value);
            
            // var bs = OpenCvSharp.BackgroundSubtractorKNN.Create(history: 100, dist2Threshold: 400);
             var bs = OpenCvSharp.BackgroundSubtractorMOG2.Create(history: 2000, varThreshold: 30, true);

            while (true)
            {
                var image = CaptureCameraCallback(bs);

                if (image != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        image.Save($"/Users/PabloMiletich/Downloads/Images/{Guid.NewGuid().ToString()}.jpg", ImageFormat.Jpeg);
                        image.Save(ms, ImageFormat.Bmp);
                        var SigBase64= Convert.ToBase64String(ms.GetBuffer()); //Get Base64
                        await UploadAsync(SigBase64);
                    }
                }

                await Task.Delay(5000);
            }
        }
       
        private static Bitmap CaptureCameraCallback(BackgroundSubtractorMOG2 bs)
        {
            var frame = new Mat();
            var capture = new VideoCapture(0);
            capture.Open(0);

            if (capture.IsOpened())
            {
                    capture.Read(frame);

                    if (_useBackgroundSubstraction){
                        //Install brew install mono-libgdiplus needed                    
                        var oa = OutputArray.Create(new Mat());

                        var dst = new Mat();

                        bs.Apply(InputArray.Create(frame), oa);

                        frame.CopyTo(dst, oa?.GetMat());

                        // SaveImage(dst.ToBitmap());
                        // SaveImage(frame.ToBitmap());
                        // SaveImage(oa?.GetMat().ToBitmap());

                        return dst.ToBitmap();
                    }

                    return frame.ToBitmap();
            }

            return null;
        }

        private static async Task UploadAsync(string image)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var content = new StringContent(JsonConvert.SerializeObject(
                        new ImageModel{
                            Image = image,
                            SpaceId = _spaceId.ToString()
                        }), Encoding.UTF8, "application/json");
                    // HTTP POST
                    HttpResponseMessage response = await client.PostAsync(_imageEndpoint, content);
                }
            }
            catch
            {
                await Task.Delay(TimeSpan.FromMinutes(1));
            }
        }

        private static async Task SaveImage(Bitmap image){
            using (var ms = new MemoryStream())
            {
                image.Save($"/Users/PabloMiletich/Downloads/Images/{Guid.NewGuid().ToString()}.jpg", ImageFormat.Jpeg);
                image.Save(ms, ImageFormat.Bmp);
                var SigBase64= Convert.ToBase64String(ms.GetBuffer()); //Get Base64
                await UploadAsync(SigBase64);
            }
        }
    }

    public class ImageModel
    {
        public string SpaceId { get; set; }
        public string Image { get; set; }
    }
}