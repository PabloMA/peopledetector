import 'package:flutter/material.dart';
import 'dart:async';

import 'Entities/AppSettings.dart';
import 'Entities/Space.dart';
import 'Services/SpaceService.dart';

class SpaceFilter extends StatefulWidget {
  final AppConfig config;

  SpaceFilter(this.config) : super();

  @override
  SpaceFilterState createState() => SpaceFilterState(this.config);
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class SpaceFilterState extends State<SpaceFilter> {
  final AppConfig config;
  final _debouncer = Debouncer(milliseconds: 500);

  List<Space> spaces = [];
  List<Space> filteredSpaces = [];

  String filteredText = "";

  SpaceFilterState(this.config);

  @override
  void initState() {
    super.initState();

    new Timer.periodic(
        Duration(seconds: 5),
        (Timer t) => {
              SpaceService.getSpaces(this.config).then((usersFromServer) {
                setState(() {
                  spaces = usersFromServer;
                  updateFilteredSpaces();
                });
              })
            });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.green),
      body: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(15.0),
              hintText: 'Find your space',
            ),
            onChanged: (string) {
              _debouncer.run(() {
                setState(() {
                  filteredText = string;
                  updateFilteredSpaces();
                });
              });
            },
          ),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(10.0),
              itemCount: filteredSpaces.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                              flex: 25,
                              child: SpaceService.getMainImage(
                                  config,
                                  filteredSpaces[index].mainImage,
                                  filteredSpaces[index].spaceId)),
                          Expanded(
                              flex: 60,
                              child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          filteredSpaces[index].name,
                                          style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Text(
                                            "${filteredSpaces[index].address} ${filteredSpaces[index].number}, ${filteredSpaces[index].city}, ${filteredSpaces[index].region}",
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.grey,
                                            )),
                                      ]))),
                          Expanded(
                              flex: 15,
                              child: MaterialButton(
                                onPressed: () {},
                                color: Colors.green,
                                textColor: Colors.white,
                                child: Text(filteredSpaces[index]
                                    .currentCount
                                    .toString()),
                                padding: EdgeInsets.all(16),
                                shape: CircleBorder(),
                              ))
                        ]),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void updateFilteredSpaces() {
    filteredSpaces = spaces
        .where((u) =>
            (u.address.toLowerCase().contains(filteredText.toLowerCase()) ||
                u.name.toLowerCase().contains(filteredText.toLowerCase()) ||
                u.region.toLowerCase().contains(filteredText.toLowerCase()) ||
                u.city.toLowerCase().contains(filteredText.toLowerCase()) ||
                u.zipCode.toLowerCase().contains(filteredText.toLowerCase())))
        .toList();
  }
}
