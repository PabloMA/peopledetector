class Space {
  int spaceId;
  int status;
  String name;
  String description;
  int currentCount;
  int maxCountLastHour;
  int maxCount;
  DateTime creationDate;
  String email;
  DateTime lastUpdate;
  String address;
  String region;
  String province;
  String city;
  String number;
  String zipCode;
  double lon;
  double lan;
  String telephone;
  String website;
  String mainImage;

  Space(
      {this.spaceId,
      this.status,
      this.name,
      this.description,
      this.currentCount,
      this.maxCountLastHour,
      this.maxCount,
      this.creationDate,
      this.email,
      this.lastUpdate,
      this.address,
      this.region,
      this.province,
      this.city,
      this.number,
      this.zipCode,
      this.lon,
      this.lan,
      this.telephone,
      this.website,
      this.mainImage});

  factory Space.fromJson(Map<String, dynamic> json) {
    return Space(
        spaceId: json["spaceId"] as int,
        status: json["status"] as int,
        name: json["name"] as String,
        description: json["description"] as String,
        currentCount: json["currentCount"] as int,
        maxCountLastHour: json["maxCountLastHour"] as int,
        maxCount: json["maxCount"] as int,
        creationDate: DateTime.parse(json['creationDate'].toString()),
        email: json["email"] as String,
        lastUpdate: DateTime.parse(json['lastUpdate'].toString()),
        address: json["address"] as String,
        region: json["region"] as String,
        province: json["province"] as String,
        city: json["city"] as String,
        number: json["number"] as String,
        zipCode: json["zipCode"] as String,
        lon: json["lon"] as double,
        lan: json["lan"] as double,
        telephone: json["telephone"] as String,
        website: json["website"] as String,
        mainImage: json["mainImage"] as String);
  }
}
