import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:longshot/Entities/AppSettings.dart';
import 'package:longshot/Entities/Space.dart';

class SpaceService {
  static const String controllerName = "Space";

  static Future<List<Space>> getSpaces(AppConfig config) async {
    try {
      final response = await http.get("${config.apiUrl}$controllerName");
      if (response.statusCode == 200) {
        List<Space> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Image getMainImage(AppConfig config, String mainImage, int spaceId) {
    return Image.network(
        ("${config.apiUrl}$controllerName/$spaceId/$mainImage"));
  }

  static List<Space> parseUsers(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Space>((json) => Space.fromJson(json)).toList();
  }
}
