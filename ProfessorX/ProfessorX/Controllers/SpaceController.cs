﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProfessorX.Services;
using System.Linq;
using ProfessorX.Entities;

namespace ProfessorX.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SpaceController : ControllerBase
    {
        private readonly ISpaceService _spaceService;
        private readonly IAppSettings _appSettings;

        public SpaceController(ISpaceService spaceService, IAppSettings appSettings)
        {
            _spaceService = spaceService;
            _appSettings = appSettings;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                var spaces = await _spaceService.GetAllSpacesAsync();

                if (spaces != null && spaces.Any())
                {
                    return Ok(spaces);
                }

                return Ok("empty");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{spaceId}/{image}")]
        public IActionResult GetImage(int spaceId, string image)
        {
            var data = _spaceService.GetSpaceImage(spaceId, image);

            return File(data.bytes, $"image/{data.extension}");
        }
    }
}
