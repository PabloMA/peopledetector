﻿using System;
using System.Drawing;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using ProfessorX.Entities;
using ProfessorX.Models;
using ProfessorX.Services;

namespace ProfessorX.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImageController : ControllerBase
    {
        private readonly ISpaceService _spaceService;
        private readonly IAppSettings _appSettings;

        public ImageController(ISpaceService spaceService, IAppSettings appSettings)
        {
            _spaceService = spaceService;
            _appSettings = appSettings;
        }

        [HttpPost]
        public IActionResult Post([FromBody] ImageModel model)
        {
            byte[] bytes = Convert.FromBase64String(model.Image);

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
                image.Save(Path.Combine(_appSettings.ProcessImagePath, $"{model.SpaceId}.jpg"));
            }

            return Ok();
        }

        [HttpGet]
        public IActionResult Get(int spaceId, int count)
        {
            _spaceService.UpdateSpaceAsync(spaceId, count);

            return Ok();
        }
    }
}
