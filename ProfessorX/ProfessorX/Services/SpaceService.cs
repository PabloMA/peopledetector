﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ProfessorX.Entities;
using ProfessorX.Models;
using ProfessorX.Repositories;

namespace ProfessorX.Services
{
    public class SpaceService : ISpaceService
    {
        private readonly ISpaceRepository _spaceRepository;
        private readonly IAppSettings _appSettings;

        public SpaceService(ISpaceRepository spaceRepository, IAppSettings appSettings)
        {
            _spaceRepository = spaceRepository;
            _appSettings = appSettings;
        }

        public Task<IEnumerable<SpaceModel>> GetAllSpacesAsync()
        {
            return _spaceRepository.GetAllSpacesAsync();
        }

        public Task UpdateSpaceAsync(int spaceId, int count)
        {
            return _spaceRepository.UpdateSpaceAsync(spaceId, count);
        }

        public (string extension, byte[] bytes) GetSpaceImage(int spaceId, string image)
        {
            var b = File.ReadAllBytes(Path.Combine(_appSettings.DefaultImagePath, spaceId.ToString(), image));
            return (Path.GetExtension(image), b);
        }
    }
}
