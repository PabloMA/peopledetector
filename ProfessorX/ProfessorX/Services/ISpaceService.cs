﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProfessorX.Models;

namespace ProfessorX.Services
{
    public interface ISpaceService
    {
        Task<IEnumerable<SpaceModel>> GetAllSpacesAsync();
        Task UpdateSpaceAsync(int spaceId, int count);
        (string extension, byte[] bytes) GetSpaceImage(int spaceId, string image);
    }
}