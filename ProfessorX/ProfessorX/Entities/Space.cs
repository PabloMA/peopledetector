﻿using System;
using ProfessorX.Enums;

namespace ProfessorX.Entities
{
    public class Space
    {
        public int SpaceId { get; set; }
        public int UserId { get; set; }
        public SpaceStatus Status { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public int CurrentCount { get; set; }
        public int MaxCountLastHour { get; set; }
        public int MaxCount { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
