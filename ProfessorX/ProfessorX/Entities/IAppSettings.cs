﻿namespace ProfessorX.Entities
{
    public interface IAppSettings
    {
        string Secret { get; set; }
        string DatabaseConnectionString { get; set; }
        string DefaultImagePath { get; set; }
        string ProcessImagePath { get; set; }
    }
}