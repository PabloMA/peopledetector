﻿namespace ProfessorX.Entities
{
    public class AppSettings : IAppSettings
    {
        public string Secret { get; set; }
        public string DatabaseConnectionString { get; set; }
        public string DefaultImagePath { get; set; }
        public string ProcessImagePath { get; set; }
    }
}
