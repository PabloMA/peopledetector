﻿namespace ProfessorX.Models
{
    public class ImageModel
    {
        public string SpaceId { get; set; }

        public string Image { get; set; }
    }
}
