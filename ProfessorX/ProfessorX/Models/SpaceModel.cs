﻿using System;
using ProfessorX.Enums;

namespace ProfessorX.Models
{
    public class SpaceModel
    {
        public int SpaceId { get; set; }
        public int UserId { get; set; }
        public SpaceStatus Status { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public int CurrentCount { get; set; }
        public int MaxCountLastHour { get; set; }
        public int MaxCount { get; set; }
        public DateTime LastUpdate { get; set; }
        public string Address { get; set; }
        public string Region { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Number { get; set; }
        public string ZipCode { get; set; }
        public float Lon { get; set; }
        public float Lat { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Website { get; set; }
        public string MainImage { get; set; }
    }
}
