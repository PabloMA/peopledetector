﻿namespace ProfessorX.Enums
{
    public enum SpaceStatus : short
    {
        Enabled = 1,
        Disabled = 2,
        Blocked = 3
    }

}
