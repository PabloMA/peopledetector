﻿using System.Threading.Tasks;
using System.Data.SqlClient;
using ProfessorX.Entities;
using ProfessorX.Models;
using Dapper;
using System.Collections.Generic;

namespace ProfessorX.Repositories
{
    public class SpaceRepository : ISpaceRepository
    {

        private string _connectionString;

        public SpaceRepository(IAppSettings settings)
        {
            _connectionString = settings.DatabaseConnectionString;
        }

        public async Task<IEnumerable<SpaceModel>> GetAllSpacesAsync()
        {
            using var con = new SqlConnection(_connectionString);

            return await con.QueryAsync<SpaceModel>(@"
SELECT 
    s.SpaceId,
    s.UserId,
    s.Status,
    s.Name,
    s.Description,
    s.CreationDate,
    s.Address,
	s.Region,
	s.Province,
	s.City,
	s.Number,
	s.ZipCode,
    s.Lat,
    s.Lon,
    s.Email,
    s.Website,
    s.Telephone,
    s.MainImage,
    sas.CurrentCount,
    sas.MaxCountLastHour,
    sas.MaxCount,
    sas.LastUpdate
FROM dbo.Spaces s
JOIN dbo.SpaceActivitySummary sas ON s.SpaceId = sas.SpaceId");
        }

        public async Task UpdateSpaceAsync(int spaceId, int count)
        {
            using var con = new SqlConnection(_connectionString);

            await con.ExecuteAsync(
                @"INSERT INTO dbo.SpaceActivity (SpaceId, Count, CreationDate) VALUES (@spaceId, @count, GETUTCDATE())",
                new { spaceId, count });

            var maxLastHour = await con.QueryAsync<int>(
                @"SELECT MAX(Count) FROM dbo.SpaceActivity WHERE SpaceId = @spaceId AND CreationDate > DATEADD(HOUR, -1, GETDATE())",
                new { spaceId, count });

            await con.ExecuteAsync(
                @"
MERGE dbo.SpaceActivitySummary t 
    USING (SELECT @spaceId as SpaceId, @count as [Count], @maxLastHour as MaxLastHour)  s
ON (s.SpaceId = t.SpaceId)
WHEN MATCHED
    THEN UPDATE SET
        t.CurrentCount = s.[Count],
        t.MaxCountLastHour = CASE WHEN s.MaxLastHour > t.MaxCountLastHour THEN s.maxLastHour ELSE t.MaxCountLastHour END,
        t.MaxCount = CASE WHEN s.[Count] > t.MaxCount THEN s.[Count] ELSE t.MaxCount END,
        t.LastUpdate = GETUTCDATE()
WHEN NOT MATCHED BY TARGET 
    THEN INSERT (SpaceId, CurrentCount, MaxCountLastHour, MaxCount, LastUpdate)
         VALUES (s.SpaceId, s.[Count], s.[Count], s.[Count], GETUTCDATE());",
            new { spaceId, count, maxLastHour });
        }

    }
}
