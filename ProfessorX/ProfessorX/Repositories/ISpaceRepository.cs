﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProfessorX.Models;

namespace ProfessorX.Repositories
{
    public interface ISpaceRepository
    {
        Task<IEnumerable<SpaceModel>> GetAllSpacesAsync();
        Task UpdateSpaceAsync(int spaceId, int count);
    }
}