﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace DatasetParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce dataset path:");
            var filePath = Console.ReadLine();

            if (File.Exists($"/Users/PabloMiletich/Downloads/{filePath}"))
            {
                var rows = File.ReadAllLines($"/Users/PabloMiletich/Downloads/{filePath}").Select(row =>
                {
                    var elements = row.Split(",");

                    return new
                    {
                        file = elements[5],
                        x = elements[1],
                        y = elements[2],
                        width = elements[3],
                        height = elements[4]
                    };

                });

                //Displaying paths
                var paths = string.Join(";", rows.Select(r => $"'Images/{r.file}'"));
                var coors = string.Join(";",
                    rows.Select(r => $"[{(r.x == "0" ? "1" : r.x)}, {(r.y == "0" ? "1" : r.y)}, {r.width}, {r.height}]"));

                var pathValues = "{" + paths + "}";
                var coorValues = coors = "{" + coors + "}";


                Console.WriteLine($"data = table({pathValues}, {coorValues})");

            }
            else
            {
                Console.Write("File does not exist");
            }


        }
    }
}
