function checkImageExport(imageLocation)
%CHECKIMAGE Summary of this function goes here
%   Detailed explanation goes here

rcnn = load('detector_2021-04-11.mat');

% Read test image
testImage = imread(imageLocation);

img = imresize(testImage,[224 224]);

% Detect stop signs
[bbox, score] = detect(rcnn.detector, img);

detectedImg = insertObjectAnnotation(img,'rectangle',bbox,score);
figure
imshow(detectedImg)
end

