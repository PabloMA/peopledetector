imageSizeTrain = [800 800 3];
numClasses = 1;
classNames = categorical({'footwear'});
params = createMaskRCNNConfig(imageSizeTrain,numClasses,classNames);


netFasterRCNN = fasterRCNNLayers(params.ImageSize,numClasses,params.AnchorBoxes,'resnet101');
dlnet = createMaskRCNN(numClasses, params);

ds = fileDatastore('Training/mixes/', 'ReadFcn',@(x)MaskReader(x));


initialLearnRate = 0.01;
momentum = 0.9;
decay = 0.0001;
velocity = [];
maxEpochs = 30;
miniBatchSize = 2;


miniBatchFcn = @(img,boxes,labels,masks) deal(cat(4,img{:}),boxes,labels,masks);



mbqTrain = minibatchqueue(ds,4, ...
    "MiniBatchFormat",["SSCB","","",""], ...
    "MiniBatchSize",miniBatchSize, ...
    "OutputCast",["single","","",""], ...
    "OutputAsDlArray",[true,false,false,false], ...
    "MiniBatchFcn",miniBatchFcn, ...
    "OutputEnvironment",["auto","cpu","cpu","cpu"]);

doTraining = true;
if doTraining
    
    iteration = 1; 
    start = tic;
    
    numEpochs = 10;
    
     % Create subplots for the learning rate and mini-batch loss
    fig = figure;
    [lossPlotter] = configureTrainingProgressPlotter(fig);
    
    % Initialize verbose output
    initializeVerboseOutput([]);
    
    % Custom training loop
    for epoch = 1:numEpochs
        reset(mbqTrain)
        shuffle(mbqTrain)
    
        while hasdata(mbqTrain)
            % Get next batch from minibatchqueue
            [X,gtBox,gtClass,gtMask] = next(mbqTrain);
        
            
            idx = dlnet.State.Parameter == "TrainedVariance";
            boundAwayFromZero = @(X) max(X, eps('single'));
            dlnet.State(idx,:) = dlupdate(boundAwayFromZero, dlnet.State(idx,:));
            
            
            % Evaluate the model gradients and loss using dlfeval
            [gradients,loss,state] = dlfeval(@networkGradients,X,gtBox,gtClass,gtMask,dlnet,params);
            dlnet.State = state;
            
            % Compute the learning rate for the current iteration
            learnRate = initialLearnRate/(1 + decay*iteration);
            
            if(~isempty(gradients) && ~isempty(loss))    
                [dlnet.Learnables,velocity] = sgdmupdate(dlnet.Learnables,gradients,velocity,learnRate,momentum);
            else
                continue;
            end
            
            displayVerboseOutputEveryEpoch(start,learnRate,epoch,iteration,loss);
                
            % Plot loss/accuracy metric
%             D = duration(0,0,toc(start),'Format','hh:mm:ss');
%             addpoints(lossPlotter,2,Iteration,double(gather(extractdata(loss))))
%             subplot(2,1,2)
%             title(strcat("Epoch: ",num2str(epoch),", Elapsed: "+string(D)))
%             drawnow
            
            iteration = iteration + 1;    
        end
    
    end
    net = dlnet;
    
    % Save the trained network
    modelDateTime = string(datetime('now','Format',"yyyy-MM-dd-HH-mm-ss"));
    save(strcat("trainedMaskRCNN-",modelDateTime,"-Epoch-",num2str(numEpochs),".mat"),'net');
    
end