function checkImageFastSlidingWindows(imageLocation, rcnn)
%CHECKIMAGE Summary of this function goes here
%   Detailed explanation goes here


% Read test image
img = imread(imageLocation);

h = 672;
w = 672;

imgSize = size(img);

img(imgSize(1):ceil(imgSize(1) / w) * w, :, :) = 0;
img(:, imgSize(2):ceil(imgSize(2) / h) * h, :) = 0;


totalBoxes = [];
totalScores = [];
totalLabels = [];

ratio = 672/224;

for x=1:ceil(size(img,1) / 672) 
    for y=1:ceil(size(img,2) / 672)
        
        indexX = (x - 1) * 672 + 1;
        indexY = (y - 1) * 672 + 1;
        
        frame = img(indexX:(indexX + w - 1), indexY:(indexY + h - 1), :); 
        
        frame = imresize(frame, [224 224]);
        
        [bbox, score, label] = detect(rcnn, frame);
        
        if (size(label,1) == 0)
            continue;
        end
        
        
        detectedImg = insertObjectAnnotation(frame,'rectangle',bbox,score);
        figure
        imshow(detectedImg)
        
        
        for i=1:size(bbox,1)            
            bbox(i,1) = bbox(i,1) * ratio + indexY;
            bbox(i,2) = bbox(i,2) * ratio + indexX;
            bbox(i,3) = bbox(i,3) * ratio;
            bbox(i,4) = bbox(i,4) * ratio;
        end
        
        totalBoxes = [totalBoxes; bbox];           
        totalScores = [totalScores; score];
        totalLabels = [totalLabels; label];
    end
end

            
detectedImg = insertObjectAnnotation(img,'rectangle',totalBoxes,totalScores);
figure
imshow(detectedImg)

end

