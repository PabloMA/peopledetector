numImageCategories = 2;
height = 408;
width = 640;
numChannels = 3;
imageSize = [width height numChannels];
inputLayer = imageInputLayer(imageSize)

filterSize = [5 5];
numFilters = 32;

middleLayers = [
convolution2dLayer(filterSize,numFilters,'Padding',2)
reluLayer()
maxPooling2dLayer(3,'Stride',2)
convolution2dLayer(filterSize,numFilters,'Padding',2)
reluLayer()
maxPooling2dLayer(3, 'Stride',2)
convolution2dLayer(filterSize,2 * numFilters,'Padding',2)
reluLayer()
maxPooling2dLayer(3,'Stride',2)
];

finalLayers = [
fullyConnectedLayer(64)
reluLayer
fullyConnectedLayer(numImageCategories)
softmaxLayer
classificationLayer
];

layers = [
    inputLayer
    middleLayers
    finalLayers
    ];

layers(2).Weights = 0.0001 * randn([filterSize numChannels numFilters]);

checkpointLocation = tempdir;

options = trainingOptions('sgdm','Verbose',false, ...
    'CheckpointPath',checkpointLocation);

rcnn = trainRCNNObjectDetector(input,layers,options);



