%https://es.mathworks.com/help/vision/ref/evaluatesemanticsegmentation.html

options = trainingOptions('sgdm', ...
  'MiniBatchSize', 32, ...
  'InitialLearnRate', 1e-6, ...
  'MaxEpochs', 10);
inputSize = [500 500 3];

layers = [
      imageInputLayer(inputSize)
      convolution2dLayer(3,16,'Stride',2,'Padding',1)
      reluLayer
      transposedConv2dLayer(2,4,'Stride',2)
      fullyConnectedLayer(2)
      softmaxLayer
      classificationLayer
      ];

rcnn = trainRCNNObjectDetector(input, layers, options, 'NegativeOverlapRange', [0 0.3]);

save('classification_2021_04_27.mat','rcnn')



