load('rcnnStopSigns.mat', 'stopSigns', 'layers')


options = trainingOptions('sgdm', ...
  'MiniBatchSize', 32, ...
  'InitialLearnRate', 1e-6, ...
  'MaxEpochs', 10);

rcnn = trainRCNNObjectDetector(ss, layers, options, 'NegativeOverlapRange', [0 0.3]);
load('rcnnStopSigns.mat', 'stopSigns', 'layers')

