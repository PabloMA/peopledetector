data = load('rcnnStopSigns.mat', 'stopSigns', 'fastRCNNLayers');

inputSize = [224 224 3];


imds = imageDatastore(input.Var1);
blds = boxLabelDatastore(input(:,2:end));

ds = combine(imds, blds);

ds = transform(ds,@(input)preprocessData(input,inputSize));

numAnchors = 3;
anchorBoxes = estimateAnchorBoxes(ds,numAnchors)
featureExtractionNetwork = resnet50;
featureLayer = 'activation_40_relu';
numClasses = 1;

lgraph = fasterRCNNLayers(inputSize,numClasses,anchorBoxes,featureExtractionNetwork,featureLayer);


options = trainingOptions('sgdm',...
    'MaxEpochs',4,...
    'MiniBatchSize',2,...
    'InitialLearnRate',1e-3,...
    'CheckpointPath',tempdir);

    [detector, info] = trainFasterRCNNObjectDetector(ds,lgraph,options, ...
        'NegativeOverlapRange',[0 0.3], ...
        'PositiveOverlapRange',[0.6 1]);
