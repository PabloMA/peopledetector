function [out] = IsPixelClose(image,x,y,number,r,g,b)

out = 0;

endX = min(size(image,1), x + number);
startX = max(x - number, 1);


endY = min(size(image,2), y + number);
startY = max(y - number, 1);


    for x=startX:endX
        for y=startY:endY            
            if (image(x,y,1) == r && image(x,y,2) == g && image(x,y,3) == b)
                out = 1;
            end            
        end
    end
end