dirBackgrounds = dir('backgrounds/*.*');
dirBackgrounds = dirBackgrounds(~[dirBackgrounds.isdir]);
imgs = GetImageLabels("images/Shoes/Sneakers and Athletic Shoes", {});

numImages = 10000;

maskRcnnDataStore = {};

targetSize = [800 800 3];

cc = 1;

for i=1:numImages
      y = randi([1 length(imgs)],1);
      x = randi([1 length(imgs{y, 3})],1);
      
      imgPath = imgs{y, 2} + "/" + imgs{y, 3}(x);
      currentImage = imread(imgPath);
      randomBackground = dirBackgrounds(randi([1 length(dirBackgrounds)],1));
      
      if(randomBackground.name == ".DS_Store")
          continue;
      end
      
      currentBackground = imread("backgrounds/" + randomBackground.name);
      
      scale = targetSize(1:2)./size(currentBackground,[1 2]);
      currentBackground = imresize(currentBackground,targetSize(1:2));
      
      [mixed, boxes, backgroundMask, footMask] = ImageMixMask(currentImage, currentBackground);
      
      if (~isempty(boxes) > 0)
          maskRcnnDataStore{1} = mixed;
          maskRcnnDataStore{2} = boxes;
          maskRcnnDataStore{3} = categorical({'footwear'});
          masks = false(size(mixed,1), size(mixed,2), 2); 
          masks(:,:,1) = footMask;
          %masks(:,:,2) = backgroundMask;
          maskRcnnDataStore{4} = masks;
          
          cc = cc + 1;
          
          save("mixes/" + cc + ".mat", 'maskRcnnDataStore');
      end
end