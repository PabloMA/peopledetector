dirBackgrounds = dir('backgrounds/*.*');
dirBackgrounds = dirBackgrounds(~[dirBackgrounds.isdir]);
imgs = dir(fullfile("images/FrontAllAnglesReducedAngles", '*.jpg'));

imgs = imgs(randperm(length(imgs)));

maxShoes = 3;
pathIndexes = {};
boxIndexes = {};

cc = 1;
i = 1;
while(1)
    
      if (i > size(imgs, 1))
          break
      end
    
      randomBackground = dirBackgrounds(randi([1 length(dirBackgrounds)],1));
      
      if(randomBackground.name == ".DS_Store")
          continue;
      end
      
      y = randi([1 length(imgs)],1);
      currentBackground = imread("backgrounds/" + randomBackground.name);      
      imagePath = "mixes/" + replace(imgs(y).name, ".jpg", "") + "_" + randomBackground.name;
      
      currentBackground = imresize(currentBackground, [780 780]);
      
      groupedBoxes = [];
      
      for z=1:randi([1 maxShoes])
          imgPath = imgs(i).folder + "/" + imgs(i).name;
          
          i = i + 1;
          
          currentImage = imread(imgPath);
          
          if (z == 1)
              base = currentBackground;
          else
              base = mixed;
          end
          
          [mixed, boxes] = ImageMixAllAnglesGreen(currentImage, base);
          
          groupedBoxes = [groupedBoxes; boxes];
          
          if (i > size(imgs, 1))
              break
          end
      end
      
      if (~isempty(boxes) > 0)
          pathIndexes{cc} = convertStringsToChars("Training/" + imagePath);
          boxIndexes{cc} = groupedBoxes;

          cc = cc + 1;
      end
      
      i = i + 1;
      
      imwrite(mixed, imagePath);
end

imageFilename = transpose(pathIndexes);
footwear = transpose(boxIndexes);
input = table(imageFilename, footwear);