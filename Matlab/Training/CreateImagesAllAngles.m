dirBackgrounds = dir('backgrounds/*.*');
dirBackgrounds = dirBackgrounds(~[dirBackgrounds.isdir]);
imgs = dir(fullfile("images/AllAngles", '*.jpg'));

imgs = imgs(randperm(length(imgs)));


maxShoes = 4;
pathIndexes = {};
boxIndexes = {};

cc = 1;

for i=1:size(imgs, 1)
      randomBackground = dirBackgrounds(randi([1 length(dirBackgrounds)],1));
      
      if(randomBackground.name == ".DS_Store")
          continue;
      end
      
      y = randi([1 length(imgs)],1);
      currentBackground = imread("backgrounds/" + randomBackground.name);      
      imagePath = "mixes/" + replace(imgs(y).name, ".jpg", "") + "_" + randomBackground.name;
      
      currentBackground = imresize(currentBackground, [500 500]);
      
      groupedBoxes = [];
      
      for z=1:randi([1 maxShoes])
          imgPath = imgs(i).folder + "/" + imgs(i).name;
          
          i = i + 1;
          
          currentImage = imread(imgPath);
          
          if (z == 1)
              base = currentBackground;
          else
              base = mixed;
          end
          
          [mixed, boxes] = ImageMixAllAngles(currentImage, base);
          
          groupedBoxes = [groupedBoxes; boxes];
          
          if (i > size(imgs, 1))
              break
          end
      end
      
      if (~isempty(boxes) > 0)
          pathIndexes{cc} = convertStringsToChars("Training/" + imagePath);
          boxIndexes{cc} = groupedBoxes;

          cc = cc + 1;
      end
      
      imwrite(mixed, imagePath);
end

imageFilename = transpose(pathIndexes);
footwear = transpose(boxIndexes);
input = table(imageFilename, footwear);