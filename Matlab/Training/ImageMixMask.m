function [mixedImage, box, backgroundMask, footMask] = ImageMixMask(image,background)
%IMAGEMIX Summary of this function goes here
%   Detailed explanation goes here

%image = imresize(image,randi([5 10],1)/10);
image = imrotate(image,randi([0 359], 1));

if (height(background) <= height(image) || width(background) <= width(image))
    mixedImage = [];
    box = [];
    backgroundMask = [];
    footMask = [];
else
    grayImage = rgb2gray(image);
    
    maxX = height(background) - height(image);
    maxY = width(background) - width(image);
    initX = randi([1 maxX],1);
    initY = randi([1 maxY],1);

    mixedImage = background;
    
    backgroundMask = false(height(background), width(background));
    footMask = false(height(background), width(background));

    minImageX = height(background);
    maxImageX = 0;
    minImageY = width(background);
    maxImageY = 0;

    for x=1:width(image(:,:,1))
        for y=1:height(image(:,:,1))
            if (grayImage(x,y) < 200)
                if (grayImage(x,y) ~= 0)
                    
                    footMask(initX + x, initY + y) = 1;
                    mixedImage(initX + x, initY + y, 1) = image(x,y,1);
                    mixedImage(initX + x, initY + y, 2) = image(x,y,2);
                    mixedImage(initX + x, initY + y, 3) = image(x,y,3);

                    if (initX + x < minImageY)
                        minImageY = initX + x;
                    end

                    if (initX + x > maxImageY)
                        maxImageY = initX + x;
                    end

                    if ( initY + y < minImageX)
                        minImageX = initY + y;
                    end

                    if ( initY + y > maxImageX)
                        maxImageX = initY + y;
                    end
                    
                    continue;
                end
            end
            
            backgroundMask(initX + x, initY + y) = 1;
        end
    end

    box = [minImageX, minImageY, (maxImageX - minImageX), ((maxImageY - minImageY))];  
end

end

