dirBackgrounds = dir('backgrounds/*.*');
dirBackgrounds = dirBackgrounds(~[dirBackgrounds.isdir]);
imgs = GetImageLabels("images/Shoes/Sneakers and Athletic Shoes", {});

numImages = 10;
maxShoes = 4;
pathIndexes = {};
boxIndexes = {};

cc = 1;

for i=1:numImages
      randomBackground = dirBackgrounds(randi([1 length(dirBackgrounds)],1));
      
      if(randomBackground.name == ".DS_Store")
          continue;
      end
      
      y = randi([1 length(imgs)],1);
      x = randi([1 length(imgs{y, 3})],1);
      currentBackground = imread("backgrounds/" + randomBackground.name);      
      imagePath = "mixes/wrong/" + replace(imgs{y, 3}(x), ".jpg", "") + "_" + randomBackground.name;
      
      groupedBoxes = [];
      
      for z=1:randi([1 maxShoes])
          y = randi([1 length(imgs)],1);
          x = randi([1 length(imgs{y, 3})],1);
          imgPath = imgs{y, 2} + "/" + imgs{y, 3}(x);
          currentImage = imread(imgPath);
          
          if (z == 1)
              base = currentBackground;
          else
              base = mixed;
          end
          
          [mixed, boxes] = ImageMix(currentImage, base);
          
          groupedBoxes = [groupedBoxes; boxes];
      end
      
      if (~isempty(boxes) > 0)
          pathIndexes{cc} = convertStringsToChars("Training/" + imagePath);
          boxIndexes{cc} = groupedBoxes;

          cc = cc + 1;
      end
      
      imwrite(mixed, imagePath);
end

imageFilename = transpose(pathIndexes);
footwear = transpose(boxIndexes);
input = table(imageFilename, footwear);