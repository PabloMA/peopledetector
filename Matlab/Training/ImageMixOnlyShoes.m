function [mixedImage, box] = ImageMixOnlyShoes(image,background)
%IMAGEMIX Summary of this function goes here
%   Detailed explanation goes here

%image = imresize(image,randi([5 10],1)/10);
%image = imrotate(image,randi([0 359], 1));

if (height(background) <= height(image) || width(background) <= width(image))
    mixedImage = [];
    box = [];
else
    grayImage = min(image, [], 3);
    binaryImage = grayImage < 210;

    maxX = 1;
    minX = size(binaryImage,1);
    maxY = 1;
    minY = size(binaryImage,2);
    
    for i=1:size(binaryImage,1)
       for z=1:size(binaryImage,2)
            if (binaryImage(i,z) == 0)
               if (maxX < i)
                  maxX = i; 
               end
               
               if (maxY < z)
                  maxY = z; 
               end
               
               if (minX > i)
                  minX = i; 
               end
               
               if (minY > z)
                  minY = z; 
               end
            end
       end
    end
    
    croppedImage = image(minX:maxX, minY:maxY,:);


%     if (randi([0 1]) < 1)
%         cut = randi([2 width(croppedImage)], 1) * 0.2;
%         
%         if (randi([0 1]) > 0)
%             croppedImage = croppedImage(:,1: cut, :);
%         else
%             croppedImage = croppedImage(:,cut:width(croppedImage), :);
%         end
%     end
   
    croppedImage = imresize(croppedImage,randi([5 10],1)/10);
    croppedImage = imrotate(croppedImage,randi([0 359], 1));
    
    if (randi([0 1]) > 0)
        croppedImage = flip(croppedImage,2);
    end    
    
    maxX = height(background) - height(croppedImage);
    maxY = width(background) - width(croppedImage);
    initX = randi([1 maxX],1);
    initY = randi([1 maxY],1);

    mixedImage = background;

    minImageX = height(background);
    maxImageX = 0;
    minImageY = width(background);
    maxImageY = 0;

    
    count  = 0;
    
    for x=1:size(croppedImage(:,:,1),1)
        for y=1:size(croppedImage(:,:,1),2)
%             if (croppedImage(x,y,1) ~= 255 && croppedImage(x,y,2) ~= 255 && croppedImage(x,y,3) ~= 255)
                 if (croppedImage(x,y,1) ~= 0 || croppedImage(x,y,2) ~= 0 || croppedImage(x,y,3) ~= 0)
                    mixedImage(initX + x, initY + y, 1) = croppedImage(x,y,1);
                    mixedImage(initX + x, initY + y, 2) = croppedImage(x,y,2);
                    mixedImage(initX + x, initY + y, 3) = croppedImage(x,y,3);

                    if (initX + x < minImageY)
                        minImageY = initX + x;
                    end

                    if (initX + x > maxImageY)
                        maxImageY = initX + x;
                    end

                    if ( initY + y < minImageX)
                        minImageX = initY + y;
                    end

                    if ( initY + y > maxImageX)
                        maxImageX = initY + y;
                    end
                    count = count + 1;
             end
         end
    end

    box = [minImageX, minImageY, (maxImageX - minImageX), ((maxImageY - minImageY))];  
end

end

