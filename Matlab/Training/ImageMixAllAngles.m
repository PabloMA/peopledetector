function [mixedImage, box] = ImageMixAllAngles(image,background)
%IMAGEMIX Summary of this function goes here
%   Detailed explanation goes here

%image = imresize(image,randi([5 10],1)/10);
%image = imrotate(image,randi([0 359], 1));

    image = imresize(image,[200 200]);

    grayImage = min(image, [], 3);
    binaryImage = grayImage < 210;
    binaryImage = bwareafilt(binaryImage, 1);
    [rows, columns] = find(binaryImage);
    row1 = min(rows);
    row2 = max(rows);
    col1 = min(columns);
    col2 = max(columns);
    % Crop
    croppedImage = image(row1:row2, col1:col2, :);
    
%     if (randi([0 1]) < 1)
%         cut = randi([2 width(croppedImage)], 1) * 0.2;
%         
%         if (randi([0 1]) > 0)
%             croppedImage = croppedImage(:,1: cut, :);
%         else
%             croppedImage = croppedImage(:,cut:width(croppedImage), :);
%         end
%     end
   
    croppedImage = imresize(croppedImage,randi([5 10],1)/10);
    croppedImage = imrotate(croppedImage,randi([0 359], 1));
    
    if (randi([0 1]) > 0)
        croppedImage = flip(croppedImage,2);
    end    
    
    grayImage = min(croppedImage, [], 3);

    
    maxX = height(background) - height(croppedImage)
    maxY = width(background) - width(croppedImage)
    
    initX = randi([1 maxX],1);
    initY = randi([1 maxY],1);

    mixedImage = background;

    minImageX = height(background);
    maxImageX = 0;
    minImageY = width(background);
    maxImageY = 0;

    for x=1:height(croppedImage(:,:,1))
        for y=1:width(croppedImage(:,:,1))
            if (grayImage(x,y) < 230)
                 if (croppedImage(x,y,1) ~= 0 || croppedImage(x,y,2) ~= 0 || croppedImage(x,y,3) ~= 0)
                    mixedImage(initX + x, initY + y, 1) = croppedImage(x,y,1);
                    mixedImage(initX + x, initY + y, 2) = croppedImage(x,y,2);
                    mixedImage(initX + x, initY + y, 3) = croppedImage(x,y,3);

                    if (initX + x < minImageY)
                        minImageY = initX + x;
                    end

                    if (initX + x > maxImageY)
                        maxImageY = initX + x;
                    end

                    if ( initY + y < minImageX)
                        minImageX = initY + y;
                    end

                    if ( initY + y > maxImageX)
                        maxImageX = initY + y;
                    end
             end
        end
    end

    box = [minImageX, minImageY, (maxImageX - minImageX), ((maxImageY - minImageY))];  
end


