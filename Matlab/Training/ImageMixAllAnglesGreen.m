function [mixedImage, box] = ImageMixAllAngles(image,background)
%IMAGEMIX Summary of this function goes here
%   Detailed explanation goes here

    hueThresholdLow = 0.32;
    hueThresholdHigh = 0.34;
    saturationThresholdLow = 0.99;
    saturationThresholdHigh = 1;
    valueThresholdLow = 0.51;
    valueThresholdHigh = 0.60;

    
    image = imresize(image,[200 200], 'Colormap', 'original');
   
%     image = imresize(image,randi([7 10],1)/10);
    image = imrotate(image,randi([0 60], 1));
    
    if (randi([0 1]) > 0)
        image = flip(image,2);
    end    
    
    maxX = height(background) - height(image)
    maxY = width(background) - width(image)
    
    initX = randi([1 maxX],1);
    initY = randi([1 maxY],1);

    mixedImage = background;

    minImageX = height(background);
    maxImageX = 0;
    minImageY = width(background);
    maxImageY = 0;

    imageHSV = rgb2hsv(image);

    hImage = imageHSV(:,:,1);
	sImage = imageHSV(:,:,2);
	vImage = imageHSV(:,:,3);
    hueMask = (hImage >= hueThresholdLow) & (hImage <= hueThresholdHigh);
    saturationMask = (sImage >= saturationThresholdLow) & (sImage <= saturationThresholdHigh);
    valueMask = (vImage >= valueThresholdLow) & (vImage <= valueThresholdHigh);            
    
    for x=1:height(image(:,:,1))
        for y=1:width(image(:,:,1))
            if (hueMask(x,y) == 0 || saturationMask(x,y) == 0 || valueMask(x,y) == 0) && IsPixelClose(image,x,y,2,1,139,2) == 0
                 if (image(x,y,1) ~= 0 || image(x,y,2) ~= 0 || image(x,y,3) ~= 0)
                    mixedImage(initX + x, initY + y, 1) = image(x,y,1);
                    mixedImage(initX + x, initY + y, 2) = image(x,y,2);
                    mixedImage(initX + x, initY + y, 3) = image(x,y,3);

                    if (initX + x < minImageY)
                        minImageY = initX + x;
                    end

                    if (initX + x > maxImageY)
                        maxImageY = initX + x;
                    end

                    if ( initY + y < minImageX)
                        minImageX = initY + y;
                    end

                    if ( initY + y > maxImageX)
                        maxImageX = initY + y;
                    end
                 end
        end
    end

    box = [minImageX, minImageY, (maxImageX - minImageX), ((maxImageY - minImageY))];  
end


