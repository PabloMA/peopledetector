dirBackgrounds = dir('backgrounds/*.*');
dirBackgrounds = dirBackgrounds(~[dirBackgrounds.isdir]);
imgs = GetImageLabels("images/Shoes/Sneakers and Athletic Shoes", {});

numImages = 1000;
maxShoes = 4;
pathIndexes = {};
boxIndexes = {};

cc = 1;

for i=1:numImages      

      currentBackground = uint8(ones(500,500,3) * 255);      
      imagePath = "mixes/" + i + ".jpg";
      
      for z=1:randi([1 maxShoes])
          y = randi([1 length(imgs)],1);
          x = randi([1 length(imgs{y, 3})],1);
          imgPath = imgs{y, 2} + "/" + imgs{y, 3}(x);
          currentImage = imread(imgPath);
          
          if (z == 1)
              base = currentBackground;
          else
              base = mixed;
          end
          
          [mixed, boxes] = ImageMixOnlyShoes(currentImage, base);

          if (~isempty(boxes) > 0)
              pathIndexes{cc} = convertStringsToChars("Training/" + imagePath);
              boxIndexes{cc} = boxes;

              cc = cc + 1;
          end
      end
      
      imwrite(mixed, imagePath);
end

input = table(transpose(pathIndexes), transpose(boxIndexes));