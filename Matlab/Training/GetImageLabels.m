function [images] = GetImageLabels(path, imageBucket)
    dirImages = dir(path);
    imageList = {};
    
    for i=1:length(dirImages)
        
       if (~strcmp(dirImages(i).name,'.') && ~strcmp(dirImages(i).name,'..') && ~strcmp(dirImages(i).name,'.DS_Store'))
            if (dirImages(i).isdir)
                imageBucket =  GetImageLabels(path + "/" + dirImages(i).name, imageBucket);
            else
                imageList{length(imageList) + 1} = dirImages(i).name;
            end
       end 
    end
    
    if (~isempty(imageList))      
        newImages = { transpose(split(path, "/")), path, imageList };
        images = [imageBucket ; newImages ];
    else
        images = imageBucket;
    end
end

