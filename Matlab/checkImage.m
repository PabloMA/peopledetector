function checkImage(imageLocation, rcnn)
%CHECKIMAGE Summary of this function goes here
%   Detailed explanation goes here


% Read test image
testImage = imread(imageLocation);

% Detect stop signs
[bboxes,score,label] = detect(rcnn,testImage,'MiniBatchSize',128);

label_str = cell(length(score), 1);

for ii=1:length(score)
    label_str{ii} = ['Confidence: ' num2str(score(ii),'%0.2f') '%'];
end


outputImage = insertObjectAnnotation(testImage, 'rectangle', bboxes, label_str);

figure
imshow(outputImage)
end

