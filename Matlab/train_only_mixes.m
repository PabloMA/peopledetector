data = load('rcnnStopSigns.mat', 'stopSigns', 'fastRCNNLayers');
fastRCNNLayers = data.fastRCNNLayers;

rng(0);
shuffledIdx = randperm(size(input,1));
footwear = input(shuffledIdx,:);

imds = imageDatastore(input.Var1);
blds = boxLabelDatastore(input(:,2:end));

ds = combine(imds, blds);

ds = transform(ds,@(input)preprocessData(input,[800 800 3]));


options = trainingOptions('sgdm', ...
    'MiniBatchSize', 10, ...
    'InitialLearnRate', 1e-3, ...
    'MaxEpochs', 10, ...
    'CheckpointPath', tempdir);

rcnn = trainFastRCNNObjectDetector(ds, fastRCNNLayers , options, ...
    'NegativeOverlapRange', [0 0.1], ...
    'PositiveOverlapRange', [0.7 1]);
