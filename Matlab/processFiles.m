function processFiles(rcnn)

import matlab.net.*
import matlab.net.http.*

url = "https://localhost:5001/Image?";

while(1)
    imagefiles = dir('FilesToProcess/*.jpg');      
    nfiles = length(imagefiles);    % Number of files found
    for ii=1:nfiles
        fileName = imagefiles(ii).folder + "/" + imagefiles(ii).name;
        img = imread(fileName);
        fileParts = split(imagefiles(ii).name, '.');
        spaceId = fileParts(1);
        img = imresize(img,[224 224]);
        [bbox, score] = detect(rcnn, img);
        count = sum(score > 0.5);

        if (size(bbox) > 0)                    
            detectedImg = insertObjectAnnotation(img,'rectangle',bbox,score);
            imwrite(detectedImg, FilesToShow + "/" + spaceId + ".jpg");            
        end
        
        r = RequestMessage;
        resp = send(r,url + "spaceId=" + spaceId + "&" + "count=" + count, HTTPOptions('CertificateFilename',''));
        
        if(resp.StatusCode < 400)
            delete(fileName);
        end
    end
    
    pause(1);
end

end

