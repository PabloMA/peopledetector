function checkImageFast(imageLocation, rcnn)
%CHECKIMAGE Summary of this function goes here
%   Detailed explanation goes here


% Read test image
img = imread(imageLocation);

img = imresize(img,[224 224]);

% Detect stop signs
[bbox, score, label] = detect(rcnn, img);

detectedImg = insertObjectAnnotation(img,'rectangle',bbox,score);
figure
imshow(detectedImg)
end

