function checkCurrentBoxes(input, index)
%CHECKIMAGE Summary of this function goes here
%   Detailed explanation goes here


% Read test image
testImage = imread(char(input{index, 1}));

outputImage = insertObjectAnnotation(testImage, 'rectangle', cell2mat(input{index, 2}), 'footwear');

figure
imshow(outputImage)
end

