function checkImageFastEdges(imageLocation, rcnn)
%CHECKIMAGE Summary of this function goes here
%   Detailed explanation goes here


% Read test image
img = imread(imageLocation);

% Detect stop signs

%  img = rgb2gray(img);

% img = uint8(edge(img, 'canny')*255);

img = imresize(img,[224 224]);

[bbox, score] = detect(rcnn, img);



detectedImg = insertObjectAnnotation(img,'rectangle',bbox,score);
figure
imshow(detectedImg)
end

