function  out = MaskReader(filename)

% Copyright 2020 The MathWorks, Inc.

    data = load(filename);
    
    out{1} = data.maskRcnnDataStore{1};
    out{2} = data.maskRcnnDataStore{2};
    out{3} = data.maskRcnnDataStore{3};
    out{4} = data.maskRcnnDataStore{4};


end